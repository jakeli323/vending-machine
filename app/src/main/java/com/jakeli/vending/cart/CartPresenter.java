package com.jakeli.vending.cart;

import android.support.annotation.NonNull;

import com.jakeli.vending.data.Item;
import com.jakeli.vending.data.Transact;
import com.jakeli.vending.data.source.ItemDataSource;
import com.jakeli.vending.data.source.ItemRepository;

import java.util.List;

/**
 * Created by lenovo on 2018/1/29.
 */

public class CartPresenter implements CartContract.Presenter{
    private ItemRepository mItemRepository;
    private CartContract.View mCartView;
    private List<Transact> mShopList;
    private int mTransactID;

    public CartPresenter(@NonNull ItemRepository itemRepository, @NonNull CartContract.View cartView, int transactID){
        mCartView = cartView;
        mItemRepository = itemRepository;
        mTransactID = transactID;

        //todo 这个商品列表应该是服务器下发的，不应该是传进来的。

        cartView.setPresenter(this);
    }

    @Override
    public void start() {
        loadShopList();
    }

    private void loadShopList(){
        mCartView.setLoadingIndicator(true);
        mItemRepository.getShopList(mTransactID, new ItemDataSource.LoadShopListCallback() {
            @Override
            public void onLoadShopListFinished(List<Transact> shopList) {
                mShopList = shopList;
                mCartView.initShopList();
                mCartView.setLoadingIndicator(false);
            }

            @Override
            public void onDataNotAvailable() {
                mCartView.setLoadingIndicator(false);
            }
        });
    }

    @Override
    public void confirmBuying(CartContract.ConfirmBuyingCallback callback) {

    }

    @Override
    public List<Transact> getShopList(){
        return mShopList;
    }

    @Override
    public String genShopListText() {
        //名字 * 4  +  名字 X  7
        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < mShopList.size(); i++){
            Transact t = mShopList.get(i);
//            t.getId();
            Item item = mItemRepository.getCachedItemById(t.getId());
            String name = item.getName();
            int count = t.getCount();

            sb.append(name);
            sb.append("   X   ");
            sb.append(count);
            sb.append("     +    ");
        }

        return sb.toString();
    }

    @Override
    public long getTotalPrice() {
        long total = 0;
        for(Transact t : mShopList){
            int id = t.getId();
            long price = mItemRepository.getCachedItemById(id).getPrice();
            int count = t.getCount();
            total += price * count;
        }
        return total;
    }

}
