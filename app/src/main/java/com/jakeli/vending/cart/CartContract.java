package com.jakeli.vending.cart;

import com.jakeli.vending.BasePresenter;
import com.jakeli.vending.BaseView;
import com.jakeli.vending.data.Transact;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lenovo on 2018/1/29.
 */

public interface CartContract{
    interface ConfirmBuyingCallback{
        void onBuyingSuccess();
        void onBuyingFail(int errorCode);
    }

    interface View extends BaseView<Presenter>{
        void initShopList();

    }

    interface Presenter extends BasePresenter{
        void confirmBuying(ConfirmBuyingCallback callback);
        List<Transact> getShopList();
        String genShopListText();
        long getTotalPrice();

    }
}
