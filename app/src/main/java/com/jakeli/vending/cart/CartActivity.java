package com.jakeli.vending.cart;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jakeli.vending.R;
import com.jakeli.vending.common.Injector;
import com.jakeli.vending.common.Util;
import com.jakeli.vending.data.Transact;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lenovo on 2018/1/22.
 */

public class CartActivity extends Activity implements CartContract.View{
    private final float displayRatio = 2f/3f;
//    private ArrayList<Transact> transacts;
    private CartContract.Presenter mPresenter;
    private TextView mShopListText;
    private TextView mTotalPriceText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        initStaticView();

        int transactID = getIntent().getIntExtra("transact_id", 0);

        new CartPresenter(Injector.provideItemRepository(), this, transactID);
        mPresenter.start();
    }

    private void initStaticView(){
        float dpWidth = Util.getDisplayRect(this).width;
        float dpHeight = Util.getDisplayRect((this)).height;

        View rootView = getLayoutInflater().inflate(R.layout.activity_cart, null, false);
        int width = Util.dpToPx(this, (int)(dpWidth * displayRatio));
        int height = Util.dpToPx(this, (int)(dpHeight * displayRatio));
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(width, height);
        setContentView(rootView, params);
        mShopListText = findViewById(R.id.text_shoplist);
        mTotalPriceText = findViewById(R.id.text_total_price);
    }

    @Override
    public void setPresenter(CartContract.Presenter presenter) {
        mPresenter = presenter;
    }


    @Override
    public void initShopList() {
//        List<Transact> shopList = mPresenter.getShopList();
//        //名字 * 4  +  名字 X  7
//
//        StringBuffer sb = new StringBuffer();
//        for(int i = 0; i < shopList.size(); i++){
//            Transact t = shopList.get(i);
//            t.getId();
//
//        }
        String str = mPresenter.genShopListText();
        mShopListText.setText(str);

        long totalPrice = mPresenter.getTotalPrice();
        mTotalPriceText.setText("价格：" + totalPrice);
    }

    @Override
    public void setLoadingIndicator(boolean isOn) {

    }
}
