package com.jakeli.vending.main;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jakeli.vending.R;
import com.jakeli.vending.common.Util;
import com.jakeli.vending.common.datatype.Rect;
import com.jakeli.vending.data.Item;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Created by lenovo on 2018/1/18.
 * item view 控制类，引用代理view
 */

public class ItemView {
    private View rootView;
    //这个context就是mainActivity，就是presenter
    private MainContract.Presenter mPresenter;

    public ItemView(Context context, ViewGroup root, LinearLayout.LayoutParams params, MainContract.Presenter presenter){
        mPresenter = presenter;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        rootView = inflater.inflate(R.layout.item_shopping_list, root, false);

        root.addView(rootView, params);

        rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addItem();
            }
        });
    }

    public void setPrice(long price){
        TextView priceText = rootView.findViewById(R.id.text_price);
        NumberFormat formatter = new DecimalFormat("#0.00");
        priceText.setText(String.valueOf(formatter.format(price)));
    }

    public View getRootView(){
        return rootView;
    }

    private void addItem(){
        int id = (int)rootView.getTag();
        mPresenter.addItem(id);
    }

}
