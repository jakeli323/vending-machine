package com.jakeli.vending.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jakeli.vending.R;
import com.jakeli.vending.common.Const;
import com.jakeli.vending.common.Injector;
import com.jakeli.vending.common.Util;
import com.jakeli.vending.common.datatype.Rect;
import com.jakeli.vending.data.Item;
import com.jakeli.vending.cart.CartActivity;

import java.util.List;


/**
 * Created by lenovo on 2018/1/14.
 */

public class MainActivity extends Activity implements MainContract.View{
    private ViewGroup rootView;
    private ViewGroup itemScrollView;
    private TextView countTextView;
    private final int kScreenMargin = 10;
    private final int kItemWidth = 250;
    private final float kWidthRatio = kItemWidth / 800f;
    private final float kItemRatio = 3 / 2;
    private final int kItemHeight = (int)(kItemWidth * kItemRatio);
    private final int kRowInterval = 10;
    private int colCount = 0;
    private float remainWidth = 0;
    private int interval = 0;
    private Rect mRect = null;
    private int mItemWidth = kItemWidth;

    private MainContract.Presenter mPresenter;

    private List<Item> itemList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initStaticView();
        new MainPresenter(Injector.provideItemRepository(), this);
        mPresenter.start();


//        initItemList();
    }

    @Override
    public void setPresenter(@NonNull MainContract.Presenter presenter){
        mPresenter = presenter;
    }

    private void initStaticView(){


        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        rootView = findViewById(R.id.main);
        itemScrollView = findViewById(R.id.item_scroll_list);
        countTextView = findViewById(R.id.text_count);
        ImageButton btn = findViewById(R.id.btn_cart);
        btn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                mPresenter.saveShopList();
            }
        });


        mRect = Util.getDisplayRect(this);
        float dpWidth = mRect.width;

        Log.d(Const.LOG_UI, "aaaaaaaaaaaaaaa  wwww  " +mRect.width);
        Log.d(Const.LOG_UI, "aaaaaaaaaaaaaaa  hhhh  " +mRect.height);
        mItemWidth = (int)(mRect.width * kWidthRatio);

        colCount = (int)(dpWidth / mItemWidth);

        Log.d(Const.LOG_UI, "colCountcolCountcolCount    " +colCount);

        remainWidth = dpWidth - mItemWidth * colCount;


        Log.d("Bind", "remain width = " + remainWidth);
        Log.d("Bind", "col count = " + colCount);

        if(colCount > 1){
            interval = (int)(remainWidth / (colCount - 1));
        }

    }

    @Override
    public void initItemList(){
        //todo 这里改成读取json 来初始化界面。

        itemList = mPresenter.getItems();

        LinearLayout row = createRow();

        for(int iter = 0; iter < itemList.size(); iter++) {
            Item itemObj = itemList.get(iter);

            int id = itemObj.getId();
            long price = itemObj.getPrice();
            String imgSrc = itemObj.getImgPath();

            int colNum = (iter + 1) % colCount;
            if(colNum == 1 && iter != 0){
                row = createRow();
            }

            int pxWidth = Util.dpToPx(this, mItemWidth);

            Log.d(Const.LOG_UI, "wwwwww = " + mRect.width);
            Log.d(Const.LOG_UI, "w ccccccc = " + mItemWidth);
            Log.d(Const.LOG_UI, "pxWidth =   " + pxWidth);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(pxWidth, LinearLayout.LayoutParams.WRAP_CONTENT);
            Log.d(Const.LOG_UI, "w intervalv = " + interval);
            if(colNum != 1){
                Log.d(Const.LOG_UI, "colNum   colNum  = " + colNum);
                Log.d(Const.LOG_UI, "lammargningingignig " );
                params.leftMargin = interval;
//                params.setMargins(10,10,10,10);

            }

            ItemView item = new ItemView(this, row, params, mPresenter);
            item.getRootView().setTag(id);
            item.setPrice(price);
            //todo 点击是事件是 item 里面处理，的，每次点击完了之后，要办东西传到mainactivity里面，
            //todo 因此真正处理点击事件的函数应该是放在mainactivity里面的。
            //todo 现在先确认mainactivity 管理点击结果的东西的数据结构
            //todo List<Transact>  Transact{mId = xxxx;  count = xxx;}
        }

        updateCountText();
    }


    private LinearLayout createRow(){
        LinearLayout.LayoutParams llParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        llParams.topMargin = kRowInterval;
        LinearLayout row = (LinearLayout)getLayoutInflater().inflate(R.layout.row_layout, itemScrollView, false);
        itemScrollView.addView(row, llParams);
        return row;
    }

    @Override
    public void cartCheckout(int transactID){
        Intent intent = new Intent(this, CartActivity.class);

        //注释掉的代码是直接activity吧shoplist传参传给另外一个activity，真正是不能这样，要经过服务器
//        Bundle b = new Bundle();
//        b.putParcelableArrayList("transacts", (ArrayList)(mPresenter.getShopList()));
//        intent.putExtras(b);

//        saveShopList
        intent.putExtra("transact_id", transactID);
        startActivity(intent);
    }


    @Override
    public void updateCountText(){
        countTextView.setText(String.valueOf(mPresenter.getTotalCount()));

    }

    @Override
    public void setLoadingIndicator(boolean isOn) {

    }


}
