package com.jakeli.vending.main;

import com.jakeli.vending.BasePresenter;
import com.jakeli.vending.BaseView;
import com.jakeli.vending.data.Item;
import com.jakeli.vending.data.Transact;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lenovo on 2018/1/28.
 */

public interface MainContract {
    interface View extends BaseView<Presenter>{
        void updateCountText();
        void initItemList();

        //用户点击“购物车”图标时候，触发。要做的操作是生成订单。
        void cartCheckout(int transactID);
    }

    interface Presenter extends BasePresenter{
        int getTotalCount();
        boolean isItemExist(int itemID);
        List<Transact> getShopList();
        Transact getItemByID(int itemID);
        void addItem(int itemID);
        List<Item> getItems();
        void saveShopList();
    }
}
