package com.jakeli.vending.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.jakeli.vending.cart.CartActivity;
import com.jakeli.vending.data.Item;
import com.jakeli.vending.data.Transact;
import com.jakeli.vending.data.source.ItemDataSource;
import com.jakeli.vending.data.source.ItemRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lenovo on 2018/1/28.
 */

public class MainPresenter implements MainContract.Presenter{
    private MainContract.View mMainView;
    private ItemRepository mItemRepository;
    private List<Transact> mShopList = new ArrayList<>();
    private List<Item> mItemList;



    //这里把 mainView 当做构造函数参数 传进来，再那这个引用调用setPresenter，而不是在客户类调一下setPresenter，
    //估计是强制客户类一定要调setPresenter方法
    public MainPresenter(@NonNull ItemRepository itemRepository, @NonNull MainContract.View mainView){
        mMainView = mainView;
        mItemRepository = itemRepository;

        mMainView.setPresenter(this);
    }

    @Override
    public void start(){
        loadItems();
    }

    private void loadItems(){
        mMainView.setLoadingIndicator(true);

        mItemRepository.getItems(new ItemDataSource.LoadItemsCallback() {
            @Override
            public void onLoadItemsFinished(List<Item> itemList) {
                mItemList = itemList;

                mMainView.initItemList();
                mMainView.setLoadingIndicator(false);
            }

            @Override
            public void onDataNotAvailable() {

            }
        });
    }

    @Override
    public void addItem(int itemID){
        if(!isItemExist(itemID)){
            Transact newTransact = new Transact(itemID, 0);
            mShopList.add(newTransact);
            Log.d("Bind", "l11111111111");
        }

        Transact t = getItemByID(itemID);
        if(t != null){
            Log.d("Bind", "22222");

            t.setCount(t.getCount() + 1);
        }

        mMainView.updateCountText();

        Log.d("Bind", "list = " + mShopList.toString());
    }

    @Override
    public Transact getItemByID(int itemID){
        for(int i = 0; i < mShopList.size(); i++){
            Transact t = mShopList.get(i);
            if(t.getId() == itemID){
                return t;
            }
        }

        return null;
    }

    @Override
    public boolean isItemExist(int itemID){
        for(int i = 0; i < mShopList.size(); i++){
            if(mShopList.get(i).getId() == itemID){
                return true;
            }
        }

        return false;
    }

    @Override
    public int getTotalCount(){
        //todo 遍历list 来统计count
        int count = 0;
        for(int i = 0; i < mShopList.size(); i++){
            Transact t = mShopList.get(i);
            count += t.getCount();
        }

        return count;
    }

    @Override
    public List<Transact> getShopList(){
        return mShopList;
    }

    @Override
    public List<Item> getItems(){
        return mItemList;
    }

    @Override
    public void saveShopList() {
        //todo 上传shoplist，到服务器，返回交易id，判断如果交易失败，触发了失败回调要显示什么，如果成功又要显示什么。

        mMainView.setLoadingIndicator(true);
        mItemRepository.saveShopList(mShopList, new ItemDataSource.SaveShopListCallback() {
            @Override
            public void onSavedShopListFinished(int transactID) {
                //todo 上传订单（shoplist)成功，返回订单id，那么打开购物车界面，并且把订单id传过去界面
                mMainView.cartCheckout(transactID);

                mMainView.setLoadingIndicator(false);
            }

            @Override
            public void onSavedShopListFailed(int errorCode) {

                //上传失败
                mMainView.setLoadingIndicator(false);
            }
        });
    }
}
