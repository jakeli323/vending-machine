package com.jakeli.vending.data.source.remote;

import android.support.annotation.NonNull;
import android.util.Log;

import com.jakeli.vending.common.Const;
import com.jakeli.vending.data.Item;
import com.jakeli.vending.data.Transact;
import com.jakeli.vending.data.source.ItemDataSource;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lenovo on 2018/1/29.
 */

public class ItemRemoteDataSource implements ItemDataSource{
    private static volatile ItemRemoteDataSource INSTANCE;
    private List<Transact> mMockShopList;
    private int mMockTransactID = 1;

    public ItemRemoteDataSource(){

    }

    public static ItemRemoteDataSource getInstance(){
        if(INSTANCE == null){
            INSTANCE = new ItemRemoteDataSource();
        }

        return INSTANCE;
    }

    @Override
    public void getItems(@NonNull LoadItemsCallback callback) {
        List<Item> itemList = new ArrayList<>();

        //***************  这里是服务器生成的********************
        JSONArray jsonArr = new JSONArray();

        for(int i = 0; i < 17; i++){
            try {
                JSONObject json = new JSONObject();
                json.put("id", i);
                json.put("price", 4L);
                json.put("count", 10);
                json.put("img_src", "@drawable/cola");
                json.put("name", "可乐_" + String.valueOf(i));
                jsonArr.put(json);
            }catch(JSONException e){
                e.printStackTrace();
            }
        }

        Log.d("Bind", "jsonArr = " + jsonArr.toString());
        //***************  这里是服务器生成的********************

        try {
            for (int iter = 0; iter < jsonArr.length(); iter++) {
                JSONObject jsonObject = (JSONObject) jsonArr.get(iter);

                int id = jsonObject.getInt("id");
                long price = jsonObject.getLong("price");
                int count = jsonObject.getInt("count");
                String imgSrc = jsonObject.getString("img_src");
                String name = jsonObject.getString("name");
                Log.d(Const.LOG_DATA, "nnnnnn djdjdjdjdjdjdj");
                Log.d(Const.LOG_DATA, "nnnnnn = " +name);
                Item item = new Item(id, name, price, imgSrc);
                itemList.add(item);
            }
        }catch(JSONException e){
            e.printStackTrace();
        }

        callback.onLoadItemsFinished(itemList);
    }


    //模拟服务器上传shoplist
    @Override
    public void saveShopList(List<Transact> shopList, SaveShopListCallback callback) {
        mMockShopList = shopList;
        mMockTransactID = 1;
        callback.onSavedShopListFinished(mMockTransactID);

        boolean mockFail = false;
        int mockErrorCode = 404;
        if(mockFail){
            //todo 这里只是模拟失败，代码不会触发，以后写到请求的回调里面
            Log.d(Const.LOG_GENERIC, "网络不能用，上传不了订单");
            callback.onSavedShopListFailed(mockErrorCode);
        }
    }

    @Override
    public void getShopList(int transactID, LoadShopListCallback callback) {
        callback.onLoadShopListFinished(mMockShopList);

        boolean mockFail = false;
        if(mockFail){
            Log.d(Const.LOG_GENERIC, "网络不能用，获取不了商品列表");
            callback.onDataNotAvailable();
        }
    }

}
