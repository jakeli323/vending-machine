package com.jakeli.vending.data;

/**
 * Created by lenovo on 2018/1/24.
 */

public class Item {
    private int mId = 0;
    private long mPrice = 0L;
    private String mName = "text_name";
    private String mImgPath;

    public Item(int id, String name, long price, String imgPath){
        mId = id;
        mName = name;
        mPrice = price;
        mImgPath = imgPath;
    }

    public int getId(){
        return mId;

    }

    public long getPrice(){
        return mPrice;
    }

    public String getName(){
        return mName;
    }

    public String getImgPath(){
        return mImgPath;
    }



}
