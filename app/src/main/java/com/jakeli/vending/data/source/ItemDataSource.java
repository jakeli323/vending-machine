package com.jakeli.vending.data.source;

import android.support.annotation.NonNull;

import com.jakeli.vending.data.Item;
import com.jakeli.vending.data.Transact;

import java.util.List;

/**
 * Created by lenovo on 2018/1/28.
 */

public interface ItemDataSource {
    interface LoadItemsCallback{
        void onLoadItemsFinished(List<Item> itemList);

        void onDataNotAvailable();
    }

    interface LoadShopListCallback{
        void onLoadShopListFinished(List<Transact> shopList);

        void onDataNotAvailable();
    }

    //每次保存（上传）购物车列表，都要服务器生成一个id，返回给客户端。
    interface SaveShopListCallback{
        void onSavedShopListFinished(int transactID);

        void onSavedShopListFailed(int errorCode);
    }

    void getItems(@NonNull LoadItemsCallback callback);

    void saveShopList(List<Transact> shopList, SaveShopListCallback saveShopListCallback);

    void getShopList(int transactID, LoadShopListCallback callback);
}
