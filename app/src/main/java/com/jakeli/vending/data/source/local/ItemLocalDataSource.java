package com.jakeli.vending.data.source.local;

import android.support.annotation.NonNull;
import android.util.Log;

import com.jakeli.vending.common.Const;
import com.jakeli.vending.data.Transact;
import com.jakeli.vending.data.source.ItemDataSource;

import java.util.List;

/**
 * Created by lenovo on 2018/1/29.
 */

public class ItemLocalDataSource implements ItemDataSource{
    private static volatile ItemLocalDataSource INSTANCE;

    public ItemLocalDataSource(){

    }

    public static ItemLocalDataSource getInstance(){
        if(INSTANCE == null){
            INSTANCE = new ItemLocalDataSource();
        }

        return INSTANCE;
    }

    @Override
    public void getItems(@NonNull LoadItemsCallback callback) {
        Log.d(Const.LOG_GENERIC, "从本地开始加载itmes数据");
    }

    @Override
    public void getShopList(int transactID, LoadShopListCallback callback) {
        // 不需要，本地获取订单没用，只有网络有用
    }

    @Override
    public void saveShopList(List<Transact> shopList, SaveShopListCallback saveShopListCallback) {
        //不需要，本地保存订单没用。用网络上传订单。
    }
}
