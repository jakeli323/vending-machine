package com.jakeli.vending.data.source;

import android.support.annotation.NonNull;
import android.util.Log;

import com.jakeli.vending.common.Const;
import com.jakeli.vending.data.Item;
import com.jakeli.vending.data.Transact;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lenovo on 2018/1/28.
 *
 *  repository的是 remote数据类单例和 local 数据单类例的封装的一个数据单例
 *
 */

public class ItemRepository implements ItemDataSource {
    private static ItemRepository INSTANCE;

    private final ItemDataSource mItemRemoteDataSource;
    private final ItemDataSource mItemLocalDataSource;

    private Map<Integer, Item> mCachedItems;

    //todo 脏数据是每次更新item时候会标记，下次客户类获取item列表时候不会返回缓存，而是从新向服务或者本地获取。
    //todo 这里用不上是因为售卖机客户端不可能改变服务器某个商品的名字价格之类的信息
//    private boolean mCacheIsDirty = false;




    //todo 考虑要不要做本地数据，因为这样，假设，没有网络，也应该可以读取本机器有多少瓶可乐
    //todo 还有第一次启动有网络，下载图片之类的，然后保存到本地持久化
    //todo 然后就算后来没有网络，启动的时候，读取本地有可乐，也应该能显示相应图片。
    //todo 因此还是得做本地持久化，也就是网络下载下来的东西得存到数据库里面。
    //todo 也就是下次没有网络时候启动，也是可以吧机器的图片读出来，显示在界面上。
    public ItemRepository (@NonNull ItemDataSource itemRemoteDataSource, @NonNull ItemDataSource itemLocalDataSource){
        mItemRemoteDataSource = itemRemoteDataSource;
        mItemLocalDataSource = itemLocalDataSource;
    }

    public static ItemRepository getInstance(ItemDataSource itemRemoteDataSource, ItemDataSource itemLocalDataSource){
        if(INSTANCE == null){
            INSTANCE = new ItemRepository(itemRemoteDataSource, itemLocalDataSource);
        }

        return INSTANCE;
    }

    @Override
    public void getShopList(int transactID, LoadShopListCallback callback) {
         mItemRemoteDataSource.getShopList(transactID, callback);
    }

    @Override
    public void saveShopList(List<Transact> shopList, SaveShopListCallback saveShopListCallback) {
        mItemRemoteDataSource.saveShopList(shopList, saveShopListCallback);
    }

    @Override
    public void getItems(@NonNull final LoadItemsCallback callback) {
        //todo 这里是优先向网络 获取数据， 网络不行了（noavailable）触发了，那么就算在本地读取。
        //todo 网络加载完了之后还得用local进行数据存储。
//        getItemsFromRemoteDataSource(callback);

        if(mCachedItems != null){
            callback.onLoadItemsFinished(new ArrayList<>(mCachedItems.values()));
            return;
        }

        //先从网络加载数据，网络不行，从本地加载。
        mItemRemoteDataSource.getItems(new LoadItemsCallback() {
            @Override
            public void onLoadItemsFinished(List<Item> itemList) {
                refreshCache(itemList);
                callback.onLoadItemsFinished(itemList);
            }

            //网络加载items不行，将从本地加载items数据
            @Override
            public void onDataNotAvailable() {
                Log.d(Const.LOG_GENERIC, "网络加载items不行，将从本地加载items数据");
                mItemLocalDataSource.getItems(new LoadItemsCallback() {
                    @Override
                    public void onLoadItemsFinished(List<Item> itemList) {
                        refreshCache(itemList);
                        callback.onLoadItemsFinished(itemList);
                    }

                    @Override
                    public void onDataNotAvailable() {
                        callback.onDataNotAvailable();
                    }
                });
            }
        });
    }


    //todo 这里这样写就是为了贪方便，因为只有在cart activity才用到这个函数。在main acitivty不能用这个。
    //todo  因为cachedItem是在main activity初始化的。
    //用这个函数之前得自己保证，cache是有东西的。
    public Item getCachedItemById(int itemId){
        if(mCachedItems != null){
            return mCachedItems.get(itemId);
        }

        return null;
    }

    private void refreshCache(List<Item> itemList){
        if(mCachedItems == null){
            mCachedItems = new LinkedHashMap<>();
        }

        mCachedItems.clear();
        for(Item item : itemList){
            mCachedItems.put(item.getId(), item);
        }

//        mCacheIsDirty = false;
    }
//
//    private void getItemsFromRemoteDataSource(@NonNull final LoadItemsCallback callback){
//        mItemRemoteDataSource.getItems(new LoadItemsCallback() {
//            @Override
//            public void onLoadItemsFinished(List<Item> itemList) {
//                callback.onLoadItemsFinished(itemList);
//            }
//
//            @Override
//            public void onDataNotAvailable() {
//                Log.d(Const.LOG_GENERIC, "data not available remotely");
//                callback.onDataNotAvailable();
//            }
//        });
//    }
//
//    private void getItemsFromLocalDataSource(@NonNull final LoadItemsCallback callback){
//
//    }
}
