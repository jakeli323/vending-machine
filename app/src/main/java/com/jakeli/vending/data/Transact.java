package com.jakeli.vending.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by lenovo on 2018/1/23.
 */

public class Transact implements Parcelable {
    private int mId;
    private int mCount = 0;

    public Transact(int mId, int mCount){
        this.mId = mId;
        this.mCount = mCount;
    }

    public Transact(Parcel in){
        this.mId = in.readInt();
        this.mCount = in.readInt();

    }

    @Override
    public String toString(){
        return " mId = " + mId + " mCount = " + mCount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeIntArray(new int[] {this.mId, this.mCount});
        dest.writeInt(mId);
        dest.writeInt(mCount);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator(){
        public Transact createFromParcel(Parcel in){
            return new Transact(in);
        }

        public Transact[] newArray(int size){
            return new Transact[size];
        }
    };

    public int getId(){
        return mId;
    }

    public int getCount(){
        return mCount;
    }

    public void setCount(int count){
        mCount = count;
    }

}