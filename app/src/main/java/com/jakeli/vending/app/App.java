package com.jakeli.vending.app;

import android.app.Application;

/**
 * Created by lenovo on 2018/1/28.
 */

public class App extends Application {
    private static App instance;

    public static App getInstance(){
        return instance;
    }


    @Override
    public void onCreate(){
        super.onCreate();
        instance = this;

    }

}
