package com.jakeli.vending;

/**
 * Created by lenovo on 2018/1/28.
 */

public interface BasePresenter {
    void start();
}
