package com.jakeli.vending.common;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Display;

import com.jakeli.vending.common.datatype.Rect;

/**
 * Created by lenovo on 2018/1/25.
 */

public class Util {

    public static Rect getDisplayRect(Activity activity){
        Display display = activity.getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float density = activity.getResources().getDisplayMetrics().density;
        float dpWidth = outMetrics.widthPixels / density;
        float dpHeight = outMetrics.heightPixels / density;

        return new Rect(dpWidth, dpHeight);
    }

    public static int pxToDp(Context context, int px) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int dp = Math.round(px / context.getResources().getDisplayMetrics().density);
        return dp;
    }

    public static int dpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int px = Math.round(dp * context.getResources().getDisplayMetrics().density);
        return px;
    }


//    public static float
}
