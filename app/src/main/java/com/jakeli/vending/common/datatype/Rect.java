package com.jakeli.vending.common.datatype;

/**
 * Created by lenovo on 2018/1/25.
 */
public class Rect {
    public float height;
    public float width;

    public Rect(float width, float height){
        this.width = width;
        this.height = height;
    }
}
