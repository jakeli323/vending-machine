package com.jakeli.vending.common;

/**
 * Created by lenovo on 2018/1/29.
 */

public class Const {
    public static final String LOG_UI = "UI";
    public static final String LOG_DATA = "DATA";
    public static final String LOG_LOGIC = "LOGIC";
    public static final String LOG_GENERIC = "GENC";
}
