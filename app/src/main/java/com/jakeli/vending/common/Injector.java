package com.jakeli.vending.common;

import android.content.Context;
import android.support.annotation.NonNull;

import com.jakeli.vending.data.source.ItemRepository;
import com.jakeli.vending.data.source.local.ItemLocalDataSource;
import com.jakeli.vending.data.source.remote.ItemRemoteDataSource;

/**
 * Created by lenovo on 2018/1/29.
 */

public class Injector {
    public static ItemRepository provideItemRepository(){
        //todo 在这里给出url等东西初始化itemrepository。 itemrepository自己往服务器拿东西。
        //todo 写成引用注入模式是为了让客户类（MainActivity）不用关心初始化model数据类过程，利于测试。
        return ItemRepository.getInstance(ItemRemoteDataSource.getInstance(), ItemLocalDataSource.getInstance());
    }
}
